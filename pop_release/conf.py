CLI_CONFIG = {
    "name": {"positional": True, "help": "The name of the software to be released",},
    "ver": {"positional": True, "help": "The version number to apply to the release",},
}
CONFIG = {}
GLOBAL = {}
SUBS = {}
DYNE = {
    "pop_release": ["pop_release"],
}
