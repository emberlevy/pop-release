===========
POP_RELEASE
===========

Pop Release is a simple tool to automate the process of creating a release.
When making POP software releases should be happening very quickly, every
few commits should justify a release.

Since releases happen so frequently, and since they should be executed in
an identical way from project to project, pop-release becomes a simple command
to update the release locally and on pypi.
